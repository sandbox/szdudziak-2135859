<?php
/**
 * @file
 * Code for user_logout module.
 *
 * @author Szymon Dudziak
 */


function user_logout_menu() {

  $items = array();
  $items['admin/config/people/logout_user'] = array(
    'title' => t('Logout user'),
    'description' => t('Logout given user.'),
    'page callback' => 'drupal_get_form',
    'access callback' => 'user_access',
    'page arguments' => array('user_logout_form'),
    'access arguments' => array('manage users sessions'),
    'type' => MENU_NORMAL_ITEM,
  );

  $items['admin/config/people/logout_user/autocomplete'] = array(
    'page callback' => 'user_logout_autocomplete',
    'access arguments' => array('manage users sessions'),
    'type' => MENU_CALLBACK
  );

  return $items;
}

function user_logout_permission() {
  return array(
    'manage users session' => array(
      'title' => t('Manage users sessions'),
      'description' => t('You can clear given user session'),
    ),
  );
}

function user_logout_form($node = null, &$form_state = null) {
  $form['user_name'] = array(
    '#title' => t('Find user'),
    '#type' => 'textfield',
    '#autocomplete_path' => 'admin/config/people/logout_user/autocomplete',
    '#required' => true,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Logout this user'),
    '#submit' => array('user_logout_submit')
  );

  return $form;
}

function user_logout_autocomplete($string) {
  $query = db_select('users', 'u');

  $or = db_or();
  $or->condition('u.name', '%' . db_like($string) . '%', 'LIKE')
    ->condition('u.mail', '%' . db_like($string) . '%', 'LIKE');

  $query->join('sessions', 's', 'u.uid = s.uid');
  $query->fields('u', array('name'));
  $query->condition($or);
  $query->range(0, 10);
  $return = $query->execute();

  // add matches to $matches
  $matches = array();
  foreach ($return as $row) {
    $matches[$row->name] = check_plain($row->name);
  }
  // return for JS
  if (!$matches) {
    $matches = new stdClass();
  }
  drupal_json_output($matches);
}

function user_logout_submit($form, &$form_state) {
  $user_name = $form_state['values']['user_name'];
  $query = db_select('users', 'u');
  $uid = $query->fields('u', array('uid'))
    ->condition('u.name', $user_name)
    ->range(0, 1)
    ->execute()
    ->fetchAssoc();

  if (intval($uid) > 0) {
    $success = db_delete('sessions')
      ->condition('uid', $uid)
      ->execute();
    if ($success) {
      drupal_set_message(t('User has been successfully logged out'));
    } else {
      drupal_set_message(t('Cannot logout user!'), 'error');
    }
  } else {
    drupal_set_message(t('User not exist!'), 'error');
  }
}