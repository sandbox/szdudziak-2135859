
-- SUMMARY --

 This module allow privileged user to logout given user.


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.



-- CONFIGURATION --

* Module doesn't require any additional configuration

-- CONTACT --


* Szymon Dudziak - http://drupal.org/user/2446620


